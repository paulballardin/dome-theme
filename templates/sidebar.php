<?php dynamic_sidebar('sidebar-primary'); ?>

<?php 
	
	if (get_field('page_large_image')) :
	
		$image = get_field('page_large_image');
		echo '<img src="'. $image['url'] .'" alt="'. $image['title'] .' " class="img-responsive featured-large">';
	endif;

	if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		the_post_thumbnail( 'full', array( 'class' => 'img-responsive featured-image' ) );
	}
?>
