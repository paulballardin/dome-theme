<?php while (have_posts()) : the_post(); ?>
<section id="work-for-us">	
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 col-sm-5 content">
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
				
			</div>
			<div class="col-lg-7 col-md-6 col-sm-7 form">
				<?php 
					echo do_shortcode( '[contact-form-7 id="413" title="Contact Us Page"]')				
					?>
				
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
