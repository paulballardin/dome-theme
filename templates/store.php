<section id="stores">
	<div class="container">	
		<div class="row">
	          		<div class="col-sm-6 equalHeight content">
		          		<header class="entry-header">
				  			<h2 class="entry-title"><?php single_post_title(); ?></h2>        
                		</header>
					<div class="entry-content">
					                <?php
						                	// Add the address shortcode
                    echo do_shortcode( '[wpsl_address]' );


                    global $post;
                    $queried_object = get_queried_object();
                    
                    
                    // Add the content
                    $post = get_post( $queried_object->ID );
                    setup_postdata( $post );
                    the_content();
                    wp_reset_postdata( $post );
                    
                    echo '<h3>Trading Hours</h3>';
                    echo do_shortcode( '[wpsl_hours]');
                    ?>
                 </div>
	          		</div>
	          		<div class="col-sm-6 equalHeight map">
		          		<?php 
		          		// Add the map shortcode
                    echo do_shortcode( '[wpsl_map]' );
                    
                    if ( has_post_thumbnail() ) {
						the_post_thumbnail('medium', array('class' => 'store_thumb img-responsive wpsl-gmap-canvas'));
					}
                    
					?>
	          		</div>
        </div>  <!-- /.row --> 
    </div><!-- /.container -->
</section>