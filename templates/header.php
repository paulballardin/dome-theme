<header class="banner" role="banner">
<div class="navbar " role="navigation">
  <div class="container-fluid"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
	    <button type="button" id="main-nav" class="navbar-toggle offcanvas-toggle" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		</button>     

		<a class="brand" href="<?php echo home_url(); ?>">
	                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dome-logo.png" alt="<?php bloginfo('name'); ?>">
	    </a>
	    <?php 
	    //$findPageId = 10;  //Set the default our coffee page
	    if( get_field('find_us_page_id', 'option') ):
  		$findPageId = get_field('find_us_page_id', 'option'); //Change the our coffee page if it has been changed in the theme options
  		endif;
  		$findDome = get_post($findPageId); 
  		
   		?>
	    <a href="/<?php echo $findDome->post_name ?>" title="Find a dome"><span class="find-dome">FIND a<br />DÔME</span></a>
	    
    </div>
        
	<nav class="nav navbar-offcanvas navbar-offcanvas-fade navbar-offcanvas-touch container  offcanvas-transform " role="navigation" id="js-bootstrap-offcanvas">
		<button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" style="float:left;">
              <span class="sr-only">Toggle navigation</span>
              <i class="glyphicon glyphicon-remove"></i>
        </button>
        <div class="offcanvas-inner navbar-default collapse navbar-collapse "> 
		
		        <?php
            if (has_nav_menu('primary_navigation')) :
        wp_nav_menu([
        		'theme_location' => 'primary_navigation', 
        		'depth'             => 2,
                'container'         => 'div',
                'container_class'   => '',
				'container_id'      => '',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'link_before' => '<span>', 
                'link_after' => '</span>', 
                'walker'            => new wp_bootstrap_navwalker()]);
                
      endif;
        ?>
		</div>
	</nav>  
    </div>

</div>
</header>
<?php
	
	if (is_front_page()) :
	get_template_part('templates/slider');
	endif;
?>



