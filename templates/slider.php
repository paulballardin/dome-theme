<?php if( have_rows('home-slider', 'option') ): ?>
<!-- Carousel
    ================================================== -->
	<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval"3600";>
		<!-- Indicators -->
		<?php $iCounter = 0; ?>
		
		<ol class="carousel-indicators">
			<?php while( have_rows('home-slider', 'option') ): the_row();
				$class = 'class=""';
				if($iCounter === 0): $class = 'class="active"'; endif;
				echo '<li data-target="#myCarousel" data-slide-to="' . $iCounter . '"' . $class . '></li>';
				$iCounter ++;
			endwhile;
		?>
		</ol>
		<div class="carousel-inner" role="listbox">
			
			<?php $counter = 0; ?>
			
			<?php while( have_rows('home-slider', 'option') ): the_row(); 
		
				$image = get_sub_field('slider-image');
				$title = get_sub_field('slider-title');
				$text = get_sub_field('slider-text');
				$link = get_sub_field('slider-link');
				$linkText = get_sub_field('slider-link-text');
				
				//Lets set the background cover position for cropping
				$sliderBackgroundPosition = 'center center';
				if (get_sub_field('slider-background-position')):
				$sliderBackgroundPosition = get_sub_field('slider-background-position');
				endif;
				
				// Lets set the caption vertical position based on the users options
				$text_position_vertical = 'bottom:20%; top: inherit;';
				if (get_sub_field('text_position_vertical')) {
				$text_position_vertical = get_sub_field('text_position_vertical');
					
					if ($text_position_vertical === 'top') {
						$text_position_vertical = 'bottom:inherit; top: 10%;';
					} else
					if ($text_position_vertical === 'middle') {
						$text_position_vertical = 'bottom:inherit; top: 50%; transform:translateY(-50%)';
					} 	
				}
				
				// Lets now set the text alignment horizontally
				$text_position_horizontal = 'left';
				if (get_sub_field('text_position_horizontal')) :
					$text_position_horizontal = get_sub_field('text_position_horizontal');
				endif;
				
				//Sets the colour of the text
				$text_color = '#ffffff';  // default colour
				if (get_sub_field('text_color')):
					$text_color = get_sub_field('text_color');
				endif;	
				
				if (!$linkText):
					$linkText = 'Find out more';
				endif;
				
				$show_text_background = 'background';
				if (!get_sub_field('show_text_background')) :
					$show_text_background = 'no-background';
				endif;

				 	
				
				?>
							
			
				<div class="item <?php if ($counter === 0): echo 'active'; endif ?> bg" style="background-image: url(<?php echo $image['url']; ?>); background-position: <?php echo $sliderBackgroundPosition ?>"> 
					<div class="carousel-caption <?php echo $show_text_background; ?> " style="<?php echo $text_position_vertical; ?>">
						<div class="container">
							<?php // check for the title
								if ($title) : 
								echo '<h1>' . $title . '</h1>';
								endif;

								//check for the text
								if ($text) : 
								echo '<p style="text-align:' . $text_position_horizontal . '; color:' . $text_color . ';">' . $text . '</p>';
								endif;
								
								//check for a button link
								if ($link) : 
								echo '<p><a class="btn btn-lg btn-primary" href="' . $link . '" role="button">' . $linkText . '</a></p>';
								endif;
								?>
						</div>
					</div>
				</div>
			
			<?php $counter ++ ; ?>
			<?php endwhile; ?>
			
			
		</div>  <!-- /.carousel-inner -->
		
		<?php /*  Hide Next Previous Controls
		<a class="left carousel-control" href="#myCarousel" role="button"
			data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
			aria-hidden="true"></span> <span class="sr-only">Previous</span>
		</a> <a class="right carousel-control" href="#myCarousel" role="button"
			data-slide="next"> <span
			class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		
		*/ ?>
	</div>
	<!-- /.carousel -->
<?php endif; ?>	


