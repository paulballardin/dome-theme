<?php  // check to see if the nutritional information exists 
 if( have_rows('downloads') ): ?>

	<section id="download-links">
		<div class="container">
			<?php if (get_field('download_links_title')):
				$title = get_field('download_links_title');
				echo '<h2>' . $title . '</h2>';
				endif; ?>
			<div class="nutrition-links">
				<div class="row row-centered">
				<?php while( have_rows('downloads') ): the_row();  ?> 
				
					<?php  // Loop through the downloads 
						if (get_sub_field('title')):
							$title = get_sub_field('title');
						endif;
						if (get_sub_field('download_link')):
							$download = get_sub_field('download_link');
							$download = $download['url'];
						elseif (get_sub_field('page_link')):
							$download = get_sub_field('page_link');
						endif;
					?>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-centered text-center nutrition-link-wrapper">
													<a class="btn btn-primary btn-brown-solid" href="<?php echo $download; ?>" title="<?php echo $download['title']; ?>"><i class="fa fa-arrow-circle-down"></i> <?php echo $title; ?></a>
							
						</div>

					
				<?php endwhile; ?>	
			</div><!-- /.nutrition-links --->
			</div> <!-- /.row --->
		</div> <!-- /.container --->
	</section>

<?php endif; ?>