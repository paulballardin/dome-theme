<?php if (get_field('video')): ?>
<section id="dome-video">
	<div class="container">
		<div class="embed-container">
			<?php the_field('video'); ?>
		</div>
	</div>
</section>

<?php endif ?>
