<?php 
	echo '<div class="page-content">';
				the_content(); 
	
	echo '</div>';?>
<?php if( have_rows('about-page-sections') ): ?>
		
			<?php while( have_rows('about-page-sections') ): the_row(); 
				echo '<section>';
					$content = get_sub_field('section_content');
					echo($content);
				echo '</section>'; 
			endwhile;
		endif;
			?>
				
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
