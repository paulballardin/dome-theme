<?php //Controls the "sections" repeater   

 if( have_rows('home_page_sections') ): ?>
	
			<?php 
				$colCounter = 1;  // Add a counter so that we can stagger the section inner col-5 and col-7 left and right 
				
				
					
					
			 	while( have_rows('home_page_sections') ): the_row(); 
			 	
			 	if ($colCounter % 2 == 0){  //used for positioning content blocks to right of left
				 	$colContentClass = 'right';
					$colImageClass = 'left';  

					} else {
					$colContentClass = 'left';
				 	$colImageClass = 'right';	
				}
				
				//get the variables we are working with
				$section_id = NULL;
				if (get_sub_field('section_id')):
					$section_id = get_sub_field('section_id');
				endif;
				$section_theme = 'theme_default';
				if (get_sub_field('section_theme')):
					$section_theme = get_sub_field('section_theme');
				endif;
				if (get_sub_field('section_title')):
				$section_title = get_sub_field('section_title');
				endif;
				if (get_sub_field('section_mobile_title')){
				$section_mobile_title = get_sub_field('section_mobile_title');
				} else {
				$section_mobile_title = get_sub_field('section_title');
				};
				if (get_sub_field('section_content')):
				$section_content = get_sub_field('section_content');
				endif;
				
				$section_link = '#';
				if(get_sub_field('section_link')) {
				$section_link = '/' . get_sub_field('section_link');
				} else 
				if (get_sub_field('section_download')) {
				$section_link = get_sub_field('section_download');	
				$section_link = $section_link['url']; 
				}
				$link_title = 'Find Out More';
				if(get_sub_field('link_title')):
				$link_title = get_sub_field('link_title');
				endif;
				
				if(get_sub_field('image')):
				$image = get_sub_field('image');
				endif;
				if(get_sub_field('image_mobile')){
				$image_mobile = get_sub_field('image_mobile');
				} else {
				$image_mobile = get_sub_field('image');	
				}
				$contentBsClass = 'col-sm-5';
				if(get_sub_field('content-bs-class')):
				$contentBsClass = get_sub_field('content-bs-class');
				endif;
				$imageBsClass = 'col-sm-7';
				if(get_sub_field('image-bs-class')):
				$imageBsClass = get_sub_field('image-bs-class');
				endif;
				$buttonClass = '';
				if ($section_theme !== 'theme_default'):
				$buttonClass = 'btn-white';
				endif;
				if (get_sub_field('button_class')):
				$buttonClass = get_sub_field('button_class');
				endif;
				$buttonIcon = '';
				if(get_sub_field('button_icon')):
				$buttonIcon = '<i class="' . get_sub_field('button_icon') . '"></i> ';
				endif;

				if(get_sub_field('image_class')):
				$image_class = get_sub_field('image_class');
				endif;
				
				$contentOrder = 'last';
				if(get_sub_field('content_order')):
				$contentOrder = get_sub_field('content_order');
				endif;
				
				
				//Apply the variables to each section
				?>
				<section class="sections <?php echo $section_theme; ?>"  id="<?php  echo $section_id; ?>">
					<div class="container">
						<div class="row">
						<?php if ($contentOrder === 'last'): ?>
							
						<div class="section-image <?php echo $imageBsClass; ?> <?php if($colImageClass): echo $colImageClass; endif; ?>">
							<a href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $section_title; ?>" class="img-responsive hidden-xs <?php echo $image_class; ?>"></a>
							<a href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><img src="<?php echo $image_mobile['url']; ?>" alt="<?php echo $section_title; ?>" class="img-responsive visible-xs <?php echo $image_class; ?>"></a>
						</div>
						<div class="section-content <?php echo $contentBsClass; ?> <?php if($colContentClass): echo $colContentClass; endif; ?>">
							<h2 class="hidden-xs"><?php echo $section_title; ?></h2>
							<h2 class="visible-xs"><?php echo $section_mobile_title; ?></h2>
							<?php echo $section_content; ?>
							
							<?php if(get_sub_field('section_link') || get_sub_field('section_download') ): ?>
							
							<a class="hidden-xs" href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><?php echo $link_title; ?></a>
							<a class="visible-xs btn btn-primary pull-left <?php echo $buttonClass; ?>"href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><?php echo $buttonIcon; ?><?php echo $link_title; ?></a>
							
							<?php endif; ?>
						</div>
						
						<?php else: ?>						
						<div class="section-content <?php echo $contentBsClass; ?> <?php if($colContentClass): echo $colContentClass; endif; ?>">
							<h2 class="hidden-xs"><?php echo $section_title; ?></h2>
							<h2 class="visible-xs"><?php echo $section_mobile_title; ?></h2>
							<?php echo $section_content; ?>
							
							<?php if(get_sub_field('section_link') || get_sub_field('section_download') ): ?>
							
							<a class="hidden-xs" href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><?php echo $link_title; ?></a>
							<a class="visible-xs btn btn-primary pull-left <?php echo $buttonClass; ?>"href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><?php echo $buttonIcon; ?><?php echo $link_title; ?></a>
							
							<?php endif; ?>
						</div>
						<div class="section-image <?php echo $imageBsClass; ?> <?php if($colImageClass): echo $colImageClass; endif; ?>">
							<a href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $section_title; ?>" class="img-responsive hidden-xs <?php echo $image_class; ?>"></a>
							<a href="<?php echo $section_link; ?>" title="<?php echo $section_title; ?>"><img src="<?php echo $image_mobile['url']; ?>" alt="<?php echo $section_title; ?>" class="img-responsive visible-xs <?php echo $image_class; ?>"></a>
						</div>


						
						<?php endif; ?>		
						
						</div><!-- /.row -->

					</div>  <!-- /.container -->
				</section>
				
				<?php $colCounter ++; ?>	
		
			<?php endwhile; ?>

	

<?php endif; ?>	
