<footer class="content-info" role="contentinfo">
  <div class="container">
	<div class="footer-social text-center">
	<a href="https://www.facebook.com/DomeCafeGroup/" target="_blank" id="dome-facebook" title="Follow us on Facebook">Follow us on Facebook</a>
	<a href="https://twitter.com/DomeCafeGroup" target="_blank" id="dome-twitter" title="Join us on Twitter">Join us on Twitter</a>
	
	</div>  
    
    <?php 
	    if( get_field('footer_text', 'option') ):
	    echo get_field('footer_text', 'option') ;
	    endif;
	    // dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
