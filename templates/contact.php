<?php while (have_posts()) : the_post(); ?>
<section id="contact">	
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 col-sm-5 content">
				<div class="beans-top">
					<div class="beans-bottom">
				<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="col-lg-7 col-md-6 col-sm-7 form">
				<?php 
					echo do_shortcode( '[contact-form-7 id="144" title="Contact Us Page"]')				
					
				?>
				
				<?php // Add popup modal for form success?>
				<!-- Modal -->
					
					<div id="myModal" class="modal " role="dialog">
					  <div class="modal-dialog">
					
					    <!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-header">
						      <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/coffeeFromAboveWithHeart.png" alt="Thank you from Dome">
					        </div>
					      <div class="modal-body">
						      <h4>Thank you!</h4>
					        <p>We really do appreciate the feedback you have given us today.</p>
					        <p class="modal-hide-small">Your comments will be delivered to and reviewed by the Dôme Guest Experience Team who will use it to improve and develop our team and our offer to you.</p>	
					        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dome-logo.png" alt="Thank you from Dome">
					      </div>
					      <div class="modal-footer">
						      <button type="button" class="close" data-dismiss="modal"></button>
					      </div>
					    </div>
					
					  </div>
					</div>
					
					
				
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
