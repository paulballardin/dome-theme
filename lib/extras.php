<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


//Add excepts to pages
add_action( 'init', __NAMESPACE__ . '\\my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


//Add Advanced Custom Fields Options Page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

function my_wpcf7_form_elements($html) {
	$text = 'Please select an option';
	$html = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $html);
	return $html;
}
add_filter('wpcf7_form_elements' , __NAMESPACE__ . '\\my_wpcf7_form_elements');


//load custom store locator template
add_filter( 'wpsl_templates', __NAMESPACE__ . '\\custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_template_directory() . '/' . 'wpsl-templates/custom-store-listing.php',
    );

    return $templates;
}



// Customise the Store Locator Search results
add_filter( 'wpsl_listing_template', __NAMESPACE__ . '\\custom_listing_template' );

function custom_listing_template() {

    global $wpsl, $wpsl_settings;
    
    $listing_template = '<li class="col-lg-3 col-md-4 col-sm-6 equalHeight" data-store-id="<%= id %>" ' . "\r\n";
    $listing_template .= "\t\t" . '<div>' . "\r\n";
	$listing_template .= "\t\t\t\t" . wpsl_store_header_template( 'listing' ) . "\r\n";
   	$listing_template .= "\t\t\t\t" . '<span> - ' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
    $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    
    // Show the phone, fax or email data if they exist.
       
    $listing_template .= "\t\t" . '</div>' . "\r\n";

	$listing_template .= "\t" . '</li>' . "\r\n";

    return $listing_template;
}





// Add the opening hours to the Popup
add_filter( 'wpsl_info_window_template', __NAMESPACE__ . '\\custom_info_window_template' );

function custom_info_window_template() {
    
    global $wpsl;
   
    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window">' . "\r\n";
    $info_window_template .= "\t\t" . '<p>' . "\r\n";
    $info_window_template .= "\t\t\t" .  wpsl_store_header_template() . "\r\n";  // Check which header format we use
    $info_window_template .= "\t\t\t" . '<span><%= address %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= address2 %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n"; // Use the correct address format
    $info_window_template .= "\t\t" . '</p>' . "\r\n";
    $info_window_template .= "\t\t" . '<% if ( phone ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'phone_label', __( 'Phone', 'wpsl' ) ) ) . '</strong>: <%= formatPhoneNumber( phone ) %></span>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t" . '<% if ( fax ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong>: <%= fax %></span>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t" . '<% if ( email ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</strong>: <%= email %></span>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    
    $info_window_template .= "\t\t" . '<% if ( hours ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<div class="wpsl-store-hours"><strong>' . esc_html( $wpsl->i18n->get_translation( 'hours_label', __( 'Hours', 'wpsl' ) ) ) . '</strong>' . "\r\n";
    $info_window_template .= "\t\t" . '<%= hours %>' . "\r\n";
    $info_window_template .= "\t\t" . '</div>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";    
    
    $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>';
    
    
    return $info_window_template;
}

//Sort serch results by alpha

/*
add_filter( 'wpsl_store_data', __NAMESPACE__ . '\\custom_result_sort' );

function custom_result_sort( $store_meta ) {
    
    $custom_sort = array();
    
    foreach ( $store_meta as $key => $row ) {
        $custom_sort[$key] = $row['store'];
    }

    array_multisort( $custom_sort, SORT_ASC, SORT_REGULAR, $store_meta );
    
    return $store_meta;
}  */

// Add GA Code

add_action('wp_head', __NAMESPACE__ . '\\hook_javascript');

function hook_javascript() {

	$output="<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27034774-1', 'auto');
  ga('send', 'pageview');
 </script>";

	echo $output;
}

/* Add HTML 5 Fall back for CF7 date select */

add_filter( 'wpcf7_support_html5_fallback', __NAMESPACE__ . '__return_true' );


