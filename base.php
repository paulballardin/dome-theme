<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
      	
  	
  	
  	$menuPageId = 12;  //Set the default menu page
  	
  	  	if( get_field('menu_page_id', 'option') ):
  		$menuPageId = get_field('menu_page_id', 'option'); //Change the menu page if it has been changed in the theme options
  	endif;
  	
  	$coffeePageId = 15;  //Set the default our coffee page
  	  	if( get_field('our_coffee_page_id', 'option') ):
  		$coffeePageId = get_field('our_coffee_page_id', 'option'); //Change the our coffee page if it has been changed in the theme options
  	endif;
  	$contactPageId = 23;  //Set the default our coffee page
  	  	if( get_field('contact_page_id', 'option') ):
  		$contactPageId = get_field('contact_page_id', 'option'); //Change the our coffee page if it has been changed in the theme options
  	endif;
  	$workPageId = 17;  //Set the default our coffee page
  	  	if( get_field('work_page_id', 'option') ):
  		$workPageId = get_field('work_page_id', 'option'); //Change the our coffee page if it has been changed in the theme options
  	endif;
  	$findPageId = 10;  //Set the defaultfind us page
  	  	if( get_field('find_us_page_id', 'option') ):
  		$findPageId = get_field('find_us_page_id', 'option'); //Change the our coffee page if it has been changed in the theme options
  	endif;
  	
  	// 1) Add in the Video Section
  	
  	if (get_field('video')):
  	get_template_part('templates/video');
  	endif;
  	
  	
  	// 2) Do Special Pages first
  	
  	if (is_page($contactPageId)):
		get_template_part('templates/contact');
	endif;	
	if 	(is_page($workPageId)) :
		get_template_part('templates/work-for-us');
	endif;	
	if (is_page($findPageId)) :
		get_template_part('templates/find-us');
	endif;
	if (get_post_type() == 'wpsl_stores' ):
		get_template_part('templates/store');
	endif;
  	
  	
  	  	
  	// 3) Add in the content and featured image
  	if (!is_page($contactPageId) && !is_page($workPageId) && !is_page($findPageId) && get_post_type() != 'wpsl_stores' ) :
	  	if ( !empty( $post) ):
		  	if( '' !== get_post()->post_content ) : ?>
		  	
				<div class="wrap container" role="document">
		      <div class="content row">
		        <main class="main equalHeight" role="main">
		          <?php include Wrapper\template_path(); ?>
		        </main><!-- /.main -->
		        <?php if (Config\display_sidebar()) : ?>
		          <aside class="sidebar equalHeight" role="complementary">
		            <?php include Wrapper\sidebar_path(); ?>
		          </aside><!-- /.sidebar -->
		        <?php endif; ?>
		      </div><!-- /.content -->
		    </div><!-- /.wrap -->
			
			<?php 
			endif;
		endif;
	endif;
  	
  	
  	
  	// 4) Add in Page Sections
  	
  	if( have_rows('home_page_sections') ):
  	 	get_template_part('templates/home-sections');
  	 endif; 
  	
  	
  	// 5) Add in the information links

  	if( have_rows('downloads') ): 
  	
  	get_template_part('templates/nutrition');
  	
  	endif;
  	
  	
  /*	
  		
		if (is_front_page() || is_page($menuPageId) || get_field('template_type') ===  'menu' ) {
		get_template_part('templates/home-sections');
		
		if (is_page($menuPageId)):
			get_template_part('templates/nutrition');
		endif;
		 
	} 	else if (is_page($coffeePageId) || get_field('template_type') ===  'coffee' ) {
		
		get_template_part('templates/video');
		get_template_part('templates/coffee');
		
	}
	else if (is_page($contactPageId)) {
		
		get_template_part('templates/contact');
				
	}
	else if (is_page($workPageId)) {
		
		get_template_part('templates/work-for-us');
				
	}
	else if (is_page($findPageId)) {
		
		get_template_part('templates/find-us');
				
	} else if (get_post_type() == 'wpsl_stores' ){
		get_template_part('templates/store');
	}	
	else {		
	//Use the Base styling for all other pages	
	?>
	
	
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main equalHeight" role="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Config\display_sidebar()) : ?>
          <aside class="sidebar equalHeight" role="complementary">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    
    <?php
	   } // end else
 */   
 

 	
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
