
          		<div class="row">
	          		<div class="col-sm-5">
		          		<header class="entry-header">
				  			<h1 class="entry-title"><?php single_post_title(); ?></h1>        
                		</header>
					<div class="entry-content">
					                <?php
						                	// Add the address shortcode
                    echo do_shortcode( '[wpsl_address]' );


                    global $post;
                    $queried_object = get_queried_object();
                    
                    
                    // Add the content
                    $post = get_post( $queried_object->ID );
                    setup_postdata( $post );
                    the_content();
                    wp_reset_postdata( $post );
                    
                                        echo do_shortcode( '[wpsl_hours]');
                    ?>
                 </div>
	          		</div>
	          		<div class="col-sm-7">
		          		<?php 
		          		// Add the map shortcode
                    echo do_shortcode( '[wpsl_map]' );
                    
					?>
	          		</div>
          		</div>