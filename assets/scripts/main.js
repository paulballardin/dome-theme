/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        $('.dropdown-toggle').dropdownHover();
    
		// Makes parent drop down clickable  
	  	$('.navbar .dropdown').hover(function() {
		$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
		
		}, function() {
		$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
		
		});
		
		$('.navbar .dropdown > a').click(function(){
		location.href = this.href;
		});
		
		//Call MatchHeight
		$('.equalHeight').matchHeight({byRow: true});
		
		
		try{Typekit.load({ async: true });}catch(e){}
		
				
				
		
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        
        	  	
	  	
      },
      finalize: function() {

        // JavaScript to be fired on the home page, after the init JS
        
        // fire this for css animations  http://www.justinaguilar.com/animations/index.html#how
     	$(window).scroll(function() {
		$('#animatedElement').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+400) {
				$(this).addClass("slideUp");
			}
		});
		});
	    	    		 
	      		 
        
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // Contact us page, note the change from contact-us to contact_us.
    'contact_us': {
      init: function() {
        // JavaScript to be fired on the contact us page

        // Fire modal on contact form sent
        var wpcf7Elm = document.querySelector( '.wpcf7' );
        $('.wpcf7-submit').on('click', function(e){
          $('.wpcf7-form-control.wpcf7-submit').hide();
          setTimeout(function(){
            $('.wpcf7-form-control.wpcf7-submit').show();
          }, 4000);       
        }); 
       document.addEventListener('wpcf7submit', function( event ) {
           // alert( "Fire!" );            
            $('#myModal').modal('show');
        }, false );
        

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
      
 		
   
   
	
 
})(jQuery); // Fully reference jQuery after this point.


	
	


		


